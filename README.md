# Bitbucket Pipelines Pipe: Azure VM Linux Script Deploy

Run scripts on [Azure Linux Virtual Machines](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/) using [Custom Script Extension Version 2](https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/custom-script-linux). Azure Linux Virtual Machines provides on-demand, high-scale, secure, virtualized infrastructure using Red Hat, Ubuntu, or the Linux distribution of your choice. The Custom Script Extension Version 2 downloads and runs scripts on Azure virtual machines. This extension is useful for post-deployment configuration, software installation, or any other configuration/management task.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: microsoft/azure-vm-linux-script-deploy:1.0.1
    variables:
      AZURE_APP_ID: '<string>'
      AZURE_PASSWORD: '<string>'
      AZURE_TENANT_ID: '<string>'
      AZURE_RESOURCE_GROUP: '<string>'
      AZURE_VM_NAME: '<string>'
      AZURE_EXTENSION_COMMAND: '<string>'
      # AZURE_EXTENSION_FILES: '<string>' # Optional
      # AZURE_FORCE_UPDATE: '<boolean>' # Optional
      # AZURE_NO_WAIT: '<boolean>' # Optional
      # AZURE_CLEANUP: '<boolean>' # Optional
      # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AZURE_APP_ID (*)         | The app ID, URL or name associated with the service principal required for login. |
| AZURE_PASSWORD (*)       | Credentials like the service principal password, or path to certificate required for login. |
| AZURE_TENANT_ID  (*)     | The AAD tenant required for login with the service principal. |
| AZURE_RESOURCE_GROUP (*) | Name of the resource group that the vm is deployed to.  |
| AZURE_VM_NAME (*)       | The name of the Virtual Machine. |
| AZURE_EXTENSION_COMMAND (*)       |  The entry point script to execute. |
| AZURE_EXTENSION_FILES                 | Comma-separated list of files to be downloaded to the virtual machine. Any local file included in this list will be uploaded to an intermediary Azure Blob Storage first.  |
| AZURE_FORCE_UPDATE                 | Force to update even if the extension configuration has not changed. Default: `false`. |
| AZURE_NO_WAIT                 | Do not wait for the long-running operation to finish. Default: `false`. |
| AZURE_CLEANUP                 | Delete intermediary Blob Storage after deployment. Default: `false`. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

You will need to configure required Azure resources before running the pipe. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Service principal

You will need a service principal with sufficient access to create an Azure App Service instance, or update an existing App Service. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

Refer to the following documentation for more detail:

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### Azure Virtual Machine

Using the service principal credentials obtained in the previous step, you can use the following commands to create an Azure Virtual Machine instance in a Bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az vm create --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_VM_NAME} --image UbuntuLTS --admin-username ${VM_USERNAME} --generate-ssh-keys
```

Refer to the following documentation for more detail:

* [Quickstart: Create a Linux virtual machine with the Azure CLI](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-cli)

## Examples

### Basic example

```yaml
script:
  - pipe: microsoft/azure-vm-linux-script-deploy:1.0.1
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_VM_NAME: $AZURE_VM_NAME
      AZURE_EXTENSION_COMMAND: 'apt-get -y update && apt-get install -y apache2'
```

### Advanced example

```yaml
script:
  - pipe: microsoft/azure-vm-linux-script-deploy:1.0.1
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      AZURE_VM_NAME: $AZURE_VM_NAME
      AZURE_EXTENSION_COMMAND: './script.sh -p my-package.zip -n default -s kestrel.service'
      AZURE_EXTENSION_FILES: 'custom-script/script.sh,my-package.zip,https://myresources/nginx/default,systemd/kestrel.service'
      AZURE_FORCE_UPDATE: 'true'
      AZURE_NO_WAIT: 'true'
      AZURE_CLEANUP: 'true'
      DEBUG: 'true'
```

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
