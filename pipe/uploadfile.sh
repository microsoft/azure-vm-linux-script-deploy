
function upload_all {
   local files=$1
   local account_name=$2
   local container_name=$3
   local result=""

   UPLOAD_RESULT=""

   # loop through all files
   IFS=',' read -ra ADDR <<< ${files}
   for i in ${ADDR[@]}; do

      URI_REGEX="^(http://|https://)\\w+"

      if [[ ${i} =~ ${URI_REGEX} ]]; then
         # file is already an uri
         # no need to upload to storage
         info "No need to upload '${i}' to Blob Container"
         FILE_URI=${i}
      elif [ -f ${i} ]; then
         # file is local
         # upload to storage and get uri
         upload_file $account_name $container_name $i
      else
         fail "File '${i}' not found" 
      fi

      UPLOAD_RESULT+="\"${FILE_URI}\","
   done

   # remove the last comma
   UPLOAD_RESULT=${UPLOAD_RESULT:: -1}
}

function upload_file {
   local account_name=$1
   local container_name=$2
   local file_path=$3

   local blob_base=${file_path##*/}
   info "File '${file_path}' basename stripped to '${blob_base}'"

   # upload file (local path specified by user) to blob container. Note: returns entity tag - identifies the current version of the object in the storage service
   info "Uploading '${blob_base}' to Blob Container '${container_name}'..."
   upload_blob=$(az storage blob upload --container-name ${container_name} --file ${file_path} --name ${blob_base} --account-name ${account_name})

   # generate a SAS token for 30 minutes
   info "Generating Shared Access Signature (SAS) token for blob '${blob_base}' for 30 minutes..."
   end=`date -d "30 minutes" '+%Y-%m-%dT%H:%MZ'`
   local sas_blob_orig=$(az storage blob generate-sas --name ${blob_base} --account-name ${account_name} --container-name ${container_name} --expiry ${end} --permissions r )
   local sas_blob=$(echo ${sas_blob_orig} | sed 's/["\t]//g')

   FILE_URI="https://${account_name}.blob.core.windows.net/${container_name}/${blob_base}?${sas_blob}"  
   info "File Uri for '${blob_base}' blob is '${FILE_URI}'"
}
