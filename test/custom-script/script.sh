# Read script parameters
while getopts p:n:s: option
do
case "${option}"
in
p) PACKAGE=${OPTARG};;
n) NGINX_CONFIG=${OPTARG};;
s) SERVICE_CONFIG=${OPTARG};;
esac
done

# Register Microsoft Key and feed
sudo wget -O packages-microsoft-prod.deb https://packages.microsoft.com/config/ubuntu/18.10/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

# Install the .NET SDK
sudo add-apt-repository universe
sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y dotnet-sdk-2.2

# Install nginx and update config file
sudo apt-get install -y nginx
sudo service nginx start
sudo cp $NGINX_CONFIG /etc/nginx/sites-available/default
sudo nginx -s reload

# Install .NET Core Web App
sudo rm -rf /var/www/webapp
sudo mkdir -p /var/www/webapp
sudo apt-get install -y unzip
sudo unzip $PACKAGE -d /var/www/webapp

#  Configure and monitor kestrel service
sudo cp $SERVICE_CONFIG /etc/systemd/system/kestrel.service
sudo systemctl enable kestrel.service
sudo systemctl start kestrel.service
