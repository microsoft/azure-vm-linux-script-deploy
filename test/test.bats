#!/usr/bin/env bats

setup() {
    DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-vm-linux-script-deploy"}

    # generated
    RANDOM_NUMBER=$RANDOM

    # required globals - stored in Pipelines repository variables
    AZURE_APP_ID="${AZURE_APP_ID}"
    AZURE_PASSWORD="${AZURE_PASSWORD}"
    AZURE_TENANT_ID="${AZURE_TENANT_ID}"

    # required globals - generated
    AZURE_RESOURCE_GROUP="test${RANDOM_NUMBER}"
    AZURE_VM_NAME="bbpipevm${RANDOM_NUMBER}"    
    AZURE_VM_ADMIN_USERNAME="username-${RANDOM_NUMBER}"
    AZURE_VM_ADMIN_PASSWORD="azureVMPassword-${RANDOM_NUMBER}"

    # locals - fixed
    AZURE_LOCATION="westus2"
    AZURE_DEPLOYMENT_TEMPLATE_FILE="test/arm-templates/VirtualMachine.json"
    AZURE_VM_MACHINE_SIZE="Standard_A2_v2"

    # locals - generated
    AZURE_VM_DNS_NAME=${AZURE_VM_NAME}
    AZURE_DEPLOYMENT_PARAMETERS="virtualMachineName=${AZURE_VM_NAME} virtualMachineSize=${AZURE_VM_MACHINE_SIZE} adminUsername=${AZURE_VM_ADMIN_USERNAME} adminPassword=${AZURE_VM_ADMIN_PASSWORD} dnsName=${AZURE_VM_DNS_NAME}"

    echo "Building image..."
    docker build -t ${DOCKER_IMAGE}:0.1.0 .

    echo "Creating required Azure resources"
    az login --service-principal --username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}
    az group create --name ${AZURE_RESOURCE_GROUP} --location ${AZURE_LOCATION}
    az group deployment create --resource-group ${AZURE_RESOURCE_GROUP} --template-file ${AZURE_DEPLOYMENT_TEMPLATE_FILE} --parameters ${AZURE_DEPLOYMENT_PARAMETERS}
}

teardown() {
    echo "Clean up Resource Group"
    az group delete -n ${AZURE_RESOURCE_GROUP} --yes
}

@test "Script can be run on Azure VM using Custom Script Extension Version 2" {
    # required globals - fixed
    AZURE_EXTENSION_COMMAND="./script.sh -p Package.zip -n default -s kestrel.service"    

    # optional globals - fixed
    AZURE_EXTENSION_FILES="test/custom-script/script.sh,test/custom-script/Package.zip,test/custom-script/nginx/default,test/custom-script/systemd/kestrel.service"

    run docker run \
        -e AZURE_APP_ID="${AZURE_APP_ID}" \
        -e AZURE_PASSWORD="${AZURE_PASSWORD}" \
        -e AZURE_TENANT_ID="${AZURE_TENANT_ID}" \
        -e AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}" \
        -e AZURE_VM_NAME="${AZURE_VM_NAME}" \
        -e AZURE_EXTENSION_COMMAND="${AZURE_EXTENSION_COMMAND}" \
        -e AZURE_EXTENSION_FILES="${AZURE_EXTENSION_FILES}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [[ "$status" -eq 0 ]]
    [[ "${output}" == *"Deployment successful."* ]]

    run curl --silent "http://${AZURE_VM_DNS_NAME}.${AZURE_LOCATION}.cloudapp.azure.com"

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"SampleWebApp"* ]]
}

